package mk.test.roomwordfab.repository;


import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

import mk.test.roomwordfab.dao.WordDao;
import mk.test.roomwordfab.database.WordRoomDatabase;
import mk.test.roomwordfab.entity.Word;

public class WordRepository {

    private WordDao wordDao;
    private LiveData<List<Word>> allWords;
    private LiveData<List<Word>> allWordsDesc;

    public WordRepository(Application application){

        WordRoomDatabase database = WordRoomDatabase.getInstance(application);
        wordDao = database.wordDao();
        allWords = wordDao.getAllWords();
        allWordsDesc = wordDao.getAllWordsDesc();
    }

    public LiveData<List<Word>> getAllWords(){
        return allWords;
    }

    public void insert(Word word){
        new InsertAsyncTask(wordDao).execute(word);
    }

    public void deleteAll(){new DeleteAllAsyncTask(wordDao).execute();}

    public void deleteItem(Word word){
        new DeleteItemAsyncTask(wordDao).execute(word);
    }

    public LiveData<List<Word>> getAllWordsDesc(){return allWordsDesc;}


    //-------------DELETE ALL ASYNC TASK
    private static class DeleteAllAsyncTask extends AsyncTask <Void, Void, Void>{
        private WordDao asyncDao;

        public DeleteAllAsyncTask(WordDao asyncDao){
            this.asyncDao = asyncDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            asyncDao.deleteAll();
            return null;
        }
    }

    //-------------INSERT ASYNC TASK
    private static class InsertAsyncTask extends AsyncTask <Word, Void, Void> {

        private WordDao asyncDao;

        public InsertAsyncTask(WordDao asyncDao){
            this.asyncDao = asyncDao;
        }

        @Override
        protected Void doInBackground(Word... words) {
            asyncDao.insert(words[0]);
            return null;
        }
    }

    //----------DELETE ITEM ASYNC TASK
    private static class DeleteItemAsyncTask extends AsyncTask <Word, Void, Void> {

        private WordDao asyncDao;

        public DeleteItemAsyncTask(WordDao asyncDao){
            this.asyncDao = asyncDao;
        }

        @Override
        protected Void doInBackground(Word... words) {
            asyncDao.deleteItem(words[0]);
            return null;
        }
    }
}