package mk.test.roomwordfab.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import mk.test.roomwordfab.interfaces.LongPressInterface;
import mk.test.roomwordfab.R;
import mk.test.roomwordfab.entity.Word;

public class WordsAdapter extends RecyclerView.Adapter<WordsAdapter.WordViewHolder> {

    private LayoutInflater inflater;
    private List<Word> words;
    private LongPressInterface longPressInterface;

    public WordsAdapter(Context context, LongPressInterface longPressInterface){
        this.inflater = LayoutInflater.from(context);
        this.longPressInterface = longPressInterface;
    }

    @NonNull
    @Override
    public WordViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType){
        View v = inflater.inflate(R.layout.item_word, parent, false);
        return new WordViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull WordViewHolder holder, int position) {
        if(words != null){
            Word word = words.get(position);
            holder.word.setText(word.getWord());
        } else {
            holder.word.setText("NO WORD");
        }
    }

    @Override
    public int getItemCount() {
        if (words != null)  {
            return words.size();
        }
        return 0;
    }

    public void setWords(List<Word> wordList){
        words = wordList;
        notifyDataSetChanged();
    }

    //---------------VIEW HOLDER----------
    public class WordViewHolder extends RecyclerView.ViewHolder {

        TextView word;

        public WordViewHolder(@NonNull View itemView) {
            super(itemView);

            word = itemView.findViewById(R.id.word_tv);

            word.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    if (longPressInterface != null){
                        longPressInterface.onLongPress(words.get(getAdapterPosition()));
                    }
                    return true;
                }
            });
        }
    }
}