package mk.test.roomwordfab.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import mk.test.roomwordfab.entity.Word;
import mk.test.roomwordfab.repository.WordRepository;

public class WordViewModel extends AndroidViewModel {

    private WordRepository wordRepository;

    public WordViewModel(@NonNull Application application) {
        super(application);

        wordRepository = new WordRepository(application);
    }

    public void insert(Word word){
        wordRepository.insert(word);
    }

    public LiveData<List<Word>> getAllWords(){
        return wordRepository.getAllWords();
    }

    public void deleteAll(){
        wordRepository.deleteAll();
    }

    public void deleteItem(Word word){
        wordRepository.deleteItem(word);
    }

    public LiveData<List<Word>> getAllWordsDesc(){
        return wordRepository.getAllWordsDesc();
    }
}