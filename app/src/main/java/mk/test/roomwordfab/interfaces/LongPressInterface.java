package mk.test.roomwordfab.interfaces;

import mk.test.roomwordfab.entity.Word;

public interface LongPressInterface {
    void onLongPress(Word word);
}