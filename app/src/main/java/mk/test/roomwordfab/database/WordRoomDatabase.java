package mk.test.roomwordfab.database;

import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import mk.test.roomwordfab.dao.WordDao;
import mk.test.roomwordfab.entity.Word;

@Database(entities = Word.class, version = 1, exportSchema = false)
public abstract class WordRoomDatabase extends RoomDatabase {

    public abstract WordDao wordDao();

    private  static WordRoomDatabase INSTANCE;

    public static WordRoomDatabase getInstance(Context context){
        if (INSTANCE == null){
            synchronized (WordRoomDatabase.class){
                if (INSTANCE == null){
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            WordRoomDatabase.class, "word_database")
                            .fallbackToDestructiveMigration()
                            .addCallback(sRoomDatabaseCallback)
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    private static RoomDatabase.Callback sRoomDatabaseCallback = new RoomDatabase.Callback(){
        @Override
        public void onOpen(@NonNull SupportSQLiteDatabase db) {
            super.onOpen(db);

            new PopulateDbAsync(INSTANCE).execute();
        }
    };

    public static class PopulateDbAsync extends AsyncTask<Void, Void, Void>{

        private final WordDao wordDao;
        private String[] words = {"Bird", "Chicken", "Dog"};

        public PopulateDbAsync(WordRoomDatabase database) {
            wordDao = database.wordDao();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            //wordDao.deleteAll();

            for (int i=0;i<words.length;i++){
                wordDao.insert(new Word(words[i]));
            }
            return null;
        }
    }
}
