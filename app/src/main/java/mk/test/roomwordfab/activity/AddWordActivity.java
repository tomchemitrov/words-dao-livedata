package mk.test.roomwordfab.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toolbar;

import java.util.List;

import mk.test.roomwordfab.R;
import mk.test.roomwordfab.entity.Word;

public class AddWordActivity extends AppCompatActivity {

    public static final String REPLY_WORD = "word";

    private EditText word;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_word);

        word = findViewById(R.id.word);
    }

    public void addWordToDb(View view) {
        Intent replyIntent = new Intent();
        if (TextUtils.isEmpty(word.getText())){
            setResult(RESULT_CANCELED, replyIntent);
        } else {
            String wordString = word.getText().toString();
            replyIntent.putExtra(REPLY_WORD, wordString);
            setResult(RESULT_OK, replyIntent);
        }
        finish();
    }
}
