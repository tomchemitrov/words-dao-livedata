package mk.test.roomwordfab.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RadioButton;
import android.widget.Toast;

import java.util.List;

import mk.test.roomwordfab.interfaces.LongPressInterface;
import mk.test.roomwordfab.R;
import mk.test.roomwordfab.adapter.WordsAdapter;
import mk.test.roomwordfab.entity.Word;
import mk.test.roomwordfab.viewmodel.WordViewModel;

public class MainActivity extends AppCompatActivity implements LongPressInterface {

    public static final int REQUEST_CODE = 1;

    private RecyclerView wordsRecyclerView;
    private WordViewModel wordViewModel;
    private WordsAdapter adapter;

    private RadioButton asc, desc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        asc = findViewById(R.id.asc_rb);
        desc = findViewById(R.id.desc_rb);

        adapter = new WordsAdapter(this, this);

        wordsRecyclerView = findViewById(R.id.recycler_view);
        wordsRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        wordsRecyclerView.setAdapter(adapter);

        wordViewModel = ViewModelProviders.of(this).get(WordViewModel.class);
        wordViewModel.getAllWords().observe(this, new Observer<List<Word>>() {
            @Override
            public void onChanged(List<Word> words) {
                adapter.setWords(words);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.clear_all) {
            new AlertDialog.Builder(this)
                    .setTitle("Delete All")
                    .setMessage("Are you sure you want to delete all entries?")

                    // Specifying a listener allows you to take an action before dismissing the dialog.
                    // The dialog is automatically dismissed when a dialog button is clicked.
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            wordViewModel.deleteAll();
                        }
                    })

                    // A null listener allows the button to dismiss the dialog and take no further action.
                    .setNegativeButton(android.R.string.no, null)
                    .show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void addWord(View view) {
        Intent intent = new Intent(this, AddWordActivity.class);
        startActivityForResult(intent, REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK){
            Word word = new Word (data.getStringExtra("word"));
            wordViewModel.insert(word);
        } else {
            Toast.makeText(this, "Word is empty", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onLongPress(final Word word) {
        //Toast.makeText(this, word.getWord() + " long pressed", Toast.LENGTH_SHORT).show();
        //wordViewModel.deleteItem(word);

        //DialogFragment dialogFragment = new VerificationFragment();
        //dialogFragment.show(getSupportFragmentManager(), "verification");

        new AlertDialog.Builder(this)
                .setTitle("Delete entry")
                .setMessage("Are you sure you want to delete this entry?")

                // Specifying a listener allows you to take an action before dismissing the dialog.
                // The dialog is automatically dismissed when a dialog button is clicked.
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        wordViewModel.deleteItem(word);
                    }
                })

                // A null listener allows the button to dismiss the dialog and take no further action.
                .setNegativeButton(android.R.string.no, null)
                .show();
    }

    public void sortAsc(View view) {
        wordViewModel.getAllWords().observe(this, new Observer<List<Word>>() {
            @Override
            public void onChanged(List<Word> words) {
                adapter.setWords(words);
                adapter.notifyDataSetChanged();
            }
        });
    }

    public void sortDesc(View view) {
        wordViewModel.getAllWordsDesc().observe(this, new Observer<List<Word>>() {
            @Override
            public void onChanged(List<Word> words) {
                adapter.setWords(words);
                adapter.notifyDataSetChanged();
            }
        });
    }
}